package com.codecool.simpleapi.ping;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class PingTest {

    private Ping ping;

    @BeforeEach
    public void setup(){
        ping = new Ping();
        ping.setStatus("OK");
        ping.setId(12);
        ping.setTimeStamp("2011-11-11");
    }

    @Test
    public void getIdTest(){
        assertEquals(ping.getId(), Integer.valueOf(12));
    }
}
package com.codecool.simpleapi.ping;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
@RequestMapping(path = "/ping")
public class PingController {

    private PingService service;

    public PingController(PingService service) {this.service = service;}

    @GetMapping(path = "")
    public Iterable<Ping> index(){
        this.service.create();
        return this.service.findAll();
    }

    @GetMapping(path = "/{id}")
    public Optional<Ping> show(@PathVariable Integer id) {return this.service.findOne(id);}

}
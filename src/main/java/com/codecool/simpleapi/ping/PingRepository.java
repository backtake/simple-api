package com.codecool.simpleapi.ping;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PingRepository extends CrudRepository<Ping, Integer> {

    Iterable<Ping> findAll();

    Ping findAllByStatus(String status);

    Optional<Ping> findById(Integer id);
}
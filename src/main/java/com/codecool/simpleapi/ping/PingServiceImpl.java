package com.codecool.simpleapi.ping;

import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Optional;

@Service
public class PingServiceImpl implements PingService {

    private PingRepository repository;

    public PingServiceImpl(PingRepository repository) {
        this.repository = repository;
    }

    @Override
    public Iterable<Ping> findAll() {
        return this.repository.findAll();
    }

    @Override
    public void save(Ping ping) {
        this.repository.save(ping);
    }

    @Override
    public Optional<Ping> findOne(Integer id) {
        return this.repository.findById(id);
    }

    @Override
    public void create() {
        Ping ping = new Ping();
        ping.setStatus("OK");
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        ping.setTimeStamp(timestamp.toString());
        save(ping);
    }
}
package com.codecool.simpleapi.ping;

import java.util.Optional;

public interface PingService {

    Iterable<Ping> findAll();
    void save(Ping ping);
    Optional<Ping> findOne(Integer id);
    void create();
}
